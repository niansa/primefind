#include "pu.hpp"

#include <iostream>
#include <chrono>
#include <commoncpp/timer.hpp>
#define THREADED_PRINT
#ifdef THREADED_PRINT
#include <commoncpp/pooled_thread.hpp>
#endif



void graphical_print(unsigned value, unsigned divisor = 1) {
    if (divisor)
        value /= divisor;

    std::cout << "\033[" << value << "C"
                 "|";
}

int main(int argc, char *argv[]) {
    const unsigned block_size = primefinder::recommended_count();
    const unsigned graph_hits_divisor = 0.00054f*block_size;
    primefinder primefinder(block_size);
    common::Timer timer;
#ifdef THREADED_PRINT
    common::PooledThread print_thread;
    print_thread.start();
#endif

    std::cout << graph_hits_divisor << std::endl;

    if (!primefinder.init()) {
        std::cerr << "Failed to initialize primefinder!" << std::endl;
        abort();
    }

    for (std::size_t current = 2; ; current += block_size) {
        timer.reset();
        const auto& result = primefinder.run(current);
        const auto elapsed = timer.get<std::chrono::milliseconds>();
        const unsigned hits = result.size();
#ifdef THREADED_PRINT
        print_thread.enqueue([graph_hits_divisor, block_size, current, elapsed, hits] () {
#endif
            std::cout << current << '-' << current+block_size-1 << " (" << hits << " hits took " << elapsed << " ms):\r\033[43C";
            graphical_print(elapsed);
            std::cout << '\r';
            graphical_print(hits, graph_hits_divisor);
            std::cout << std::endl;
#ifdef THREADED_PRINT
        });
#endif
    }
}
