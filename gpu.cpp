#include "pu.hpp"

#include <iostream>
#include <vector>
#include <hip/hip_runtime.h>

#if __HIP_DEVICE_COMPILE__
#define __inline_on_device__ __inline__
#else
#define __inline_on_device__
#endif



__device__ __host__ __inline_on_device__ bool is_prime(std::size_t value) noexcept {
    if (value % 2 == 0)
        return false;

    for (int i = 3; i*i <= value; i += 2)
        if (value % i == 0)
            return false;

    return true;
}

__global__ static void primefind_gpu(bool *out, std::size_t start) noexcept {
    const unsigned value = threadIdx.x + blockDim.x * blockIdx.x;
    out[value] = is_prime(std::size_t(value)+start);
}


primefinder::primefinder(unsigned count)
    : count(count) {}

primefinder::~primefinder() {
    if (deviceBuffer != nullptr)
        hipFree(deviceBuffer);
}

bool primefinder::init() {
    if (deviceBuffer != nullptr)
        return false;
    if (hipMalloc(&deviceBuffer, count) != hipSuccess)
        return false;
    hostBuffer.resize(count, false);
    return true;
}

const std::vector<std::size_t>& primefinder::run(std::size_t start) noexcept {
    hipLaunchKernelGGL(primefind_gpu, dim3(32), dim3(count/32), 0, 0, deviceBuffer, start);

    hipMemcpy(reinterpret_cast<void*>(hostBuffer.data()), deviceBuffer, count, hipMemcpyDeviceToHost);

    resultBuffer.clear();
    for (unsigned it = 0; it != count; it++)
        if (hostBuffer[it])
            resultBuffer.push_back(std::size_t(it)+start);

    return resultBuffer;
}

unsigned primefinder::recommended_count() {
    return 16129;
}
