#ifndef GPU_HPP
#define GPU_HPP
#include <vector>

#ifndef __HIPCC__
#define __device__
#define __host__
#endif


struct primefinder {
    unsigned count;
    bool *deviceBuffer = nullptr;
    std::vector<char> hostBuffer;
    std::vector<std::size_t> resultBuffer;

public:
    /// Cronstruct prime finder
    ///
    /// \arg count amount of numbers to crunch at a time (on HIP: must be divisible by 32, max 16129)
    primefinder(unsigned count);
    ~primefinder();

    /// Initialize prime finder
    ///
    /// Allocates buffers used throughout the lifetime of this class instance.
    /// May only be called once
    ///
    /// \returns true if successful, otherwise false
    bool init();

    /// Finds all prime numbers within given window
    ///
    /// \arg start number to start at
    ///
    /// \returns statically allocated vector containing all numbers that were found
    const std::vector<std::size_t>& run(std::size_t start) noexcept;

    static unsigned recommended_count();
};


/// Check if number is prime
///
/// \arg value number to check (must be above 1)
///
/// \returns true if prime, otherwise false
__device__ __host__ bool is_prime(std::size_t value) noexcept;
#endif // GPU_HPP
