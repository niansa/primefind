#include "pu.hpp"

#include <iostream>
#include <vector>



bool is_prime(std::size_t value) noexcept {
    if (value % 2 == 0)
        return false;

    for (int i = 3; i*i <= value; i += 2)
        if (value % i == 0)
            return false;

    return true;
}

static inline void primefind_cpu(bool *out, std::size_t start, unsigned offset) noexcept {
    out[offset] = is_prime(std::size_t(offset)+start);
}


primefinder::primefinder(unsigned count)
    : count(count) {}

primefinder::~primefinder() {}

bool primefinder::init() {
    hostBuffer.resize(count, false);
    return true;
}

const std::vector<std::size_t>& primefinder::run(std::size_t start) noexcept {
    auto hostBufferPtr = reinterpret_cast<bool*>(hostBuffer.data());

    #pragma omp parallel for
    for (unsigned offset = 0; offset != count; offset++)
        primefind_cpu(hostBufferPtr, start, offset);

    resultBuffer.clear();
    for (unsigned it = 0; it != count; it++)
        if (hostBufferPtr[it])
            resultBuffer.push_back(std::size_t(it)+start);

    return resultBuffer;
}

unsigned primefinder::recommended_count() {
    return 60000;
}
